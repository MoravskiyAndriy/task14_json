package com.moravskiyandriy.model;

public class Blade {
    private double bladeLength;
    private double bladeWidth;
    private String bladeMaterial;

    public Blade() {
    }

    public Blade(double bladeLength, double bladeWidth, String bladeMaterial) {
        this.bladeLength = bladeLength;
        this.bladeWidth = bladeWidth;
        this.bladeMaterial = bladeMaterial;
    }

    public double getBladeLength() {
        return bladeLength;
    }

    public void setBladeLength(double bladeLength) {
        this.bladeLength = bladeLength;
    }

    public double getBladeWidth() {
        return bladeWidth;
    }

    public void setBladeWidth(double bladeWidth) {
        this.bladeWidth = bladeWidth;
    }

    public String getBladeMaterial() {
        return bladeMaterial;
    }

    public void setBladeMaterial(String bladeMaterial) {
        this.bladeMaterial = bladeMaterial;
    }

    @Override
    public String toString() {
        return "Blade{" +
                "bladeLength=" + bladeLength +
                ", bladeWidth=" + bladeWidth +
                ", bladeMaterial='" + bladeMaterial + '\'' +
                '}';
    }
}
