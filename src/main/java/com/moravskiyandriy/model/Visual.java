package com.moravskiyandriy.model;

public class Visual {
    private Blade blade;
    private HandMaterial hand;
    private boolean hasRigidityRib;

    public Visual() {
    }

    public Visual(Blade blade, HandMaterial hand, boolean hasRigidityRib) {
        this.blade = blade;
        this.hand = hand;
        this.hasRigidityRib = hasRigidityRib;
    }

    public Blade getBlade() {
        return blade;
    }

    public void setBlade(Blade blade) {
        this.blade = blade;
    }

    public HandMaterial getHand() {
        return hand;
    }

    public void setHand(String hand) {
        this.hand = HandMaterial.valueOf(hand);
    }

    public boolean isHasRigidityRib() {
        return hasRigidityRib;
    }

    public void setHasRigidityRib(boolean hasRigidityRib) {
        this.hasRigidityRib = hasRigidityRib;
    }

    @Override
    public String toString() {
        return "{" +
                "blade=" + blade +
                ", hand=" + hand +
                ", hasRigidityRib=" + hasRigidityRib +
                '}';
    }
}
