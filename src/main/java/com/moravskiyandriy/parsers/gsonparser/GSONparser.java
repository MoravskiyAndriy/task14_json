package com.moravskiyandriy.parsers.gsonparser;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.gson.Gson;
import com.moravskiyandriy.model.ColdWeapon;
import com.moravskiyandriy.validation.JSONValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Optional;
import java.util.Properties;

public class GSONparser {
    private static final Logger logger = LogManager.getLogger(GSONparser.class);
    private static final String JSON_KNIVES_PATH = "src\\main\\resources\\json\\knives.json";
    private static final String JSON_SCHEMA_PATH = "src\\main\\resources\\json\\JSONschema.json";

    public static void main(String[] args) {
        Properties prop = getProperties();
        String jsonFilePath = Optional.ofNullable(prop.
                getProperty("JSON_KNIVES_PATH")).
                orElse(JSON_KNIVES_PATH);
        String jsonSchemaPath = Optional.ofNullable(prop.
                getProperty("JSON_SCHEMA_PATH")).
                orElse(JSON_SCHEMA_PATH);
        boolean isValid = false;
        isValid = isValidJSONFile(jsonFilePath, jsonSchemaPath, isValid);
        if (isValid) {
            Gson gson = new Gson();
            ColdWeapon[] cwArray = new ColdWeapon[]{};
            try (Reader reader = new FileReader("src\\main\\resources\\json\\knives.json")) {
                cwArray = gson.fromJson(reader, ColdWeapon[].class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (ColdWeapon cw : cwArray) {
                logger.info(cw);
            }
        }
    }

    private static boolean isValidJSONFile(String jsonFilePath, String jsonSchemaPath, boolean isValid) {
        try {
            isValid = new JSONValidator().Validate(jsonFilePath, jsonSchemaPath);
        } catch (IOException e) {
            logger.info("IO exception occurred.");
            e.printStackTrace();
        } catch (ProcessingException e) {
            logger.info("Processing exception occurred.");
            e.printStackTrace();
        }
        return isValid;
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = GSONparser.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
