package com.moravskiyandriy.validation;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.File;
import java.io.IOException;

public class JSONValidator {
    public boolean Validate(String jsonFilePath, String schemaFilePath) throws IOException, ProcessingException {
        File schemaFile = new File(schemaFilePath);
        File jsonFile = new File(jsonFilePath);
        return ValidationUtils.isJsonValid(schemaFile, jsonFile);
    }
}
